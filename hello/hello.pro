#-------------------------------------------------
#
# Project created by QtCreator 2019-06-09T09:51:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = hello
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

DISTFILES += \
    qss/bootstrap.qss

RESOURCES += \
    hello.qrc
