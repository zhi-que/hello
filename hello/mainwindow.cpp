#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTextEdit>

#include<QFile>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QStringList  list;

    list.append(tr("选项1"));
    list.append(tr("选项2"));
    list.append(tr("选项3"));
    list.append(tr("选项4"));

    ui->comboBox->addItems(list);
    ui->comboBox_2->addItems(list);
    ui->comboBox_3->addItems(list);
    ui->comboBox_4->addItems(list);
    ui->comboBox_5->addItems(list);
    ui->comboBox_6->addItems(list);
    ui->comboBox_7->addItems(list);
    ui->comboBox_8->addItems(list);
    ui->comboBox_9->addItems(list);

//    QFile *qssfile=new QFile(":/qss/bootstrap.qss",this);
//    qssfile->open(QFile::ReadOnly);
//    QString stylesheet=tr(qssfile->readAll());
//    setStyleSheet(stylesheet);
//    qssfile->close();
}

MainWindow::~MainWindow()
{
    delete ui;
}

