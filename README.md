# QtHello

### 介绍

> Qt widget皮肤库
>
> ![运行截图](https://gitee.com/ZhiQueCangSheng/hello/raw/master/current.png)

### 使用教程

> 1. 为要使用该皮肤库的控件添加字符串类型的动态属性”styleClass“ ![添加动态属性](https://gitee.com/ZhiQueCangSheng/hello/raw/master/addStyleClass.png)
> 2. 选择窗体右键，选择更改样式表，粘贴qss目录下的bootstrap.qss的文件内容 ![应用样式](https://gitee.com/ZhiQueCangSheng/hello/raw/master/changeStyle.png)
> 3. 对要美化控件填写styleClass属性的值，多个值以空格分隔  ![设置styleClass值](https://gitee.com/ZhiQueCangSheng/hello/raw/master/setStyleValue.png)
> 4. 调整控件大小，调整完成后按钮和文本框类控件的高度和宽度要额外增加4px以适应获取焦点时边框的变化

###  已经美化的控件

QPushButton,QLineEdit,QMenu,QMenuBar，QCombobox

### styleClass值

> text-*：设置控件中字体颜色，若text-white设置字体颜色为白色,
>
> > + text-muted
> > + text-primary
> > + text-success
> > + text-info
> > + text-warning
> > + text-danger
> > + text-secondary
> > + text-dark
> > + text-light
> > + text-white
>
> bg-*：设置控件背景颜色
>
> > + bg-primary
> > + bg-success
> > + bg-info
> > + bg-warning
> > + bg-danger
> > + bg-secondary
> > + bg-dark
> > + bg-light
>
> btn-*：QPushButton的额外样式
>
> > + btn-primary
> > + btn-secondary
> > + btn-success
> > + btn-info
> > + btn-warning
> > + btn-danger
> > + btn-dark
> > + btn-light
> > + btn-link
> > + btn-lg:大按钮
> > + btn-sm:小按钮
>
> btn-outline-*：QPushButton外边框类按钮样式
>
> > + btn-outline-primary
> > + btn-outline-secondary
> > + btn-outline-success
> > + btn-outline-info
> > + btn-outline-warning
> > + btn-outline-danger
> > + btn-outline-dark
> > + btn-outline-light

> select-*:QComboBox的样式
>
> > + select-primary
> > + select-secondary
> > + select-info
> > + select-success
> > + select-warning
> > + select-danger
> > + select-dark
> > + select-light



